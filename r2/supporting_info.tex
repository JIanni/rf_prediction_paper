\documentclass[11pt]{article}
% Command line: pdflatex -shell-escape LT_Pulse.tex
%\usepackage{geometry} 
\usepackage[margin=0.5in]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{times}
\usepackage{bm}
\usepackage{fixltx2e}
\usepackage[outerbars]{changebar}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{color}
\usepackage{tabularx}
\usepackage{pdflscape}

\epstopdfsetup{suffix=} % to remove 'eps-to-pdf' suffix from converted images
%\usepackage{todonotes} % use option disable to hide all comments

\usepackage[sort&compress]{natbib}
\bibpunct{[}{]}{,}{n}{,}{,}

\usepackage[noend]{algpseudocode}

\usepackage{dsfont}
\usepackage{relsize}
\usepackage{gensymb}

%changing the Eq. tag to use [] when numbering. use \eqref{label} to reference equations in text.
\makeatletter
  \def\tagform@#1{\maketag@@@{[#1]\@@italiccorr}}
\makeatother

\linespread{1}
%\setlength{\parindent}{0in}

% the following command can be used to mark changes made due to reviewers' concerns. Use as \revbox{Rx.y} for reviewer x, concern y.
\newcommand{\revbox}[1]{\marginpar{\framebox{#1}}}
%\newcommand{\revbox}[1]{}

%\newcommand{\bop}{$\vert B_1^+ \vert$}
\newcommand{\kt}{$k_\textrm{T}$}
\newcommand{\bone}{$B_1^+$}
\newcommand{\bmap}{$B_1^+$}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}  % tilde symbol
\mathchardef\mhyphen="2D

\begin{document}



\newpage

\pagenumbering{gobble}

\title{{\bf Supporting Information:} Machine Learning RF Shimming:\\ Prediction by Iteratively Projected Ridge Regression}
\author{Julianna D Ianni$^{1,2}$, Zhipeng Cao$^{1,2}$ and William A Grissom$^{1,2,3,4}$}
\maketitle
\begin{flushleft}
$^1$Vanderbilt University Institute of Imaging Science \\
$^2$Department of Biomedical Engineering \\
$^3$Department of Radiology \\
$^4$Department of Electrical Engineering, Vanderbilt University, Nashville, TN, United States \\[1em]
Corresponding author: \\
Will Grissom\\
Vanderbilt University Institute of Imaging Science\\
1161 21st Avenue South\\
Medical Center North, AA-1105\\
Nashville, TN 37235 USA \\
E-mail: will.grissom@vanderbilt.edu \\
Twitter: @wgrissom
\end{flushleft}
\thispagestyle{plain}

\pagebreak

\renewcommand\thesection{S\arabic{section}.}
\renewcommand{\theequation}{S\arabic{equation}}
\renewcommand\thefigure{S\arabic{figure}} 

\renewcommand\thefigure{S\arabic{figure}} 
\setcounter{figure}{0}  

\begin{landscape}
\begin{figure}
\centering
\includegraphics[width=1.3\textwidth]{S_threePlane.png}
\caption{$B_1^+$ patterns produced for all the axial slices of a representative Duke head model
by circularly polarized (CP) mode, DD shims, NN shims,  
and PIPRR shims.
The slices are positioned at -4, 0, and +4 cm in each dimension.
$B_1^+$ CoVs over all slices are reported for each method, 
as well as minimum and maximum $B_1^+$.
White arrows indicate regions where the Nearest-Neighbor shims had significantly low or high $B_1^+$,
and black arrows indicate significant $B_1^+$ discontinuities between adjacent axial slices.}
\label{SthreePlane}
\end{figure}
\end{landscape}

\begin{landscape}
\begin{figure}
\centering
\includegraphics[width=1.3\textwidth]{S_separated}
\caption{
a) Global SARs for all test slices for circularly polarized (CP) mode, 
DD, NN, 
KRR applied to the DD shims, 
and PIPRR (PIPRR Test). 
PIPRR training shim SARs are also shown.
The values are normalized to the mean global SAR of the DD shims.
b) The corresponding maximum local 10-gram SARs for all test slices (where the maximum was calculated over the entire head),
normalized to the mean maximum local 10-gram SAR of the DD shims.}
\label{Sseparated}
\end{figure}
\end{landscape}

\begin{landscape}
\begin{figure}
\centering
\includegraphics[width=1.3\textwidth]{S_duke10gSAR}
\caption{
Local 10-gram SAR maps of Direct Design, Nearest Neighbor, and PIPRR shims
for a central slice of a Duke model, 
where the three methods had similar $B_1^+$ CoV's (0.015, 0.017, and 0.021, respectively).
The maps were calculated assuming a 1 ms, 1 degree average flip angle excitation using a Gaussian RF pulse shape, 
with a 1 second repetition time.}
\label{Sduke10gSAR}
\end{figure}
\end{landscape}

\begin{landscape}
\begin{figure}
\centering
\includegraphics[width=1.3\textwidth]{S_ella10gSAR}
\caption{
Local 10-gram SAR maps of Direct Design, Nearest Neighbor, and PIPRR shims
for a central slice of a Ella model, 
where the three methods had similar $B_1^+$ CoV's (0.017, 0.018, and 0.021, respectively).
The maps were calculated assuming a 1 ms, 1 degree average flip angle excitation using a Gaussian RF pulse shape, 
with a 1 second repetition time.}
\label{Sella10gSAR}
\end{figure}
\end{landscape}

\begin{figure}
\centering
\includegraphics[width=6in]{S_train}
\caption{The amplitude of the SAR penalty term (a.u.)
for one fold's test set slices,
when varying the number of heads used to train PIPRR.
The SAR penalty amplitude of the predicted shims is comparable to that of shims predicted with the full 90-head training set 
when at least 60 heads are included in the learning.}
\label{Strain}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=6in]{S_noise}
\caption{SAR penalty amplitude (a.u.)
of all test set shims,
with noise of varied amplitude added to the features used for PIPRR prediction. 
Noise level is reported in terms of equivalent $B_1$ map SNR. 
The no-noise case is indicated by SNR $=\infty$.}
\label{Snoise}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=6in]{S_feats}
\caption{SAR penalty amplitude (a.u.) of all test set shims, 
as feature groups are accrued into the final KRR weight learning and testing, 
in order of importance. 
The number of features in each class is reported in parentheses next to the class label.
Importance was measured as the norm of the KRR weights on each feature class 
over a range of KRR regularization parameters. 
Cross-terms of mask centroids, $B_1^+$ DC Fourier coefficients, 
Fourier shape descriptors (FSDs), 
and slice position were the most important features.}
\label{Sfeatimp}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=6in]{S_coils}
\caption{SAR performance of PIPRR test set shims predicted using incomplete sets of $B_1^+$ map DC coefficients. 
Shown are box plots of the SAR penalty amplitude (a.u.) of all test set shims,
versus the number of coils whose DC Fourier coefficients were included in PIPRR training and testing. }
\label{Sskipcoils}
\end{figure}

\end{document}
